package homework13;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int evenNums = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                evenNums++;
            }
        }
        int[] result = new int[evenNums];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                result[index] = array[i];
                index++;
            }
        }
        return result;
    }
}