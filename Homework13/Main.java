package homework13;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	    int[] array = {788, 98, 63, 866, 453, 552};
        int[] a = Sequence.filter(array, number -> number % 2 == 0);
        int[] b = Sequence.filter(array, numberDigitSum -> {
            int sum = 0;
            while (numberDigitSum != 0) {
                sum += numberDigitSum % 10;
                numberDigitSum /= 10;
            }
            return sum % 2 == 0;
        });
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
    }
}
