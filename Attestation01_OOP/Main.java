package attestation01;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        System.out.println("Введите ID пользователя: ");
        List<User> users = usersRepository.findById(scanner.nextInt());

        for (User user : users) {
            System.out.println(user.getId() + " " + user.getName() + " " + user.getAge() + " " + user.getIsWorker());
                user.setName("Марсель");
                user.setAge(27);
                usersRepository.update(user);
            }
    }
}
