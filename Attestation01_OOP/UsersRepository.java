package attestation01;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();
    List<User> findById(int Id);
    void update(User user);

}
