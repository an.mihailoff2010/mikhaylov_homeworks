package attestation01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {
    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем Id
                int Id = Integer.parseInt(parts[0]);
                // берем имя
                String name = parts[1];
                // берем возраст
                int age = Integer.parseInt(parts[2]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                // создаем нового человека
                User newUser = new User(Id, name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    @Override
    public List<User> findById(int Id) {
        List<User> filtered = new ArrayList<>();
        for (User u : findAll()) {
            if (u.getId() == Id) {
                filtered.add(u);
            } else {
                System.out.println("Такой записи не существует");
            }
        }
        return filtered;
    }

    @Override
    public void update(User user) {
        try {
            Reader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            ArrayList<String[]> list = new ArrayList<>();
            while (line != null) {
                String[] parts = line.split("\\|");
                if (Integer.parseInt(parts[0]) == (user.getId())) {
                    parts[1] = user.getName();
                    parts[2] = user.getAge() + "";
                    parts[3] = user.getIsWorker() + "";
                }
                list.add(parts);
                line = bufferedReader.readLine();
                Writer writer = new FileWriter("users.txt", false);
                BufferedWriter bufferedWriter = new BufferedWriter(writer);
                for (String[] newParts : list) {
                    bufferedWriter.write(parts[0] + "|" + parts[1] + "|" + parts[2] + "|" + parts[3] + "\n");
                    newParts = parts;
                    bufferedWriter.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
