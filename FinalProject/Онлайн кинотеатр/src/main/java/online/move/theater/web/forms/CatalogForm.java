package online.move.theater.web.forms;

import lombok.Data;

@Data
public class CatalogForm {
    private Integer id;
    private String name;
    private Integer year_of_production;
    private String style;
    private String country;
    private String duration;
    private String description;
}
