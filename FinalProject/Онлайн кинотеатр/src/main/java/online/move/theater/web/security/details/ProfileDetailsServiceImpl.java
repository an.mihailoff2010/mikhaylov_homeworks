package online.move.theater.web.security.details;

import lombok.RequiredArgsConstructor;
import online.move.theater.web.models.Profile;
import online.move.theater.web.repositories.ProfilesRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProfileDetailsServiceImpl implements UserDetailsService {

    private final ProfilesRepository profilesRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Profile profile = profilesRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Profile not found"));
        ProfilesDetailsImpl profilesDetails = new ProfilesDetailsImpl(profile);
        return profilesDetails;
    }
}
