package online.move.theater.web.repositories;

import online.move.theater.web.models.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfilesRepository extends JpaRepository<Profile, Integer> {

    Optional<Profile> findByEmail(String email);
}
