package online.move.theater.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class BrowsingHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String viewingOfTime;

    @ManyToOne
    @JoinColumn(name = "catalog_id")
    public Catalog catalog;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    public Profile profile;
}
