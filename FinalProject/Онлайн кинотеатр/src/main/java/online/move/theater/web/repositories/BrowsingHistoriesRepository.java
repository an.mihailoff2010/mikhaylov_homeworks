package online.move.theater.web.repositories;

import online.move.theater.web.models.BrowsingHistory;
import online.move.theater.web.models.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BrowsingHistoriesRepository extends JpaRepository<BrowsingHistory, Integer> {
    List<BrowsingHistory> findAllByProfile_IdAndCatalog_Id(Integer profileId, Integer catalogId);
    List<BrowsingHistory> findAll();
}
