package online.move.theater.web.forms;

import lombok.Data;

@Data
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String sex;
    private String telephoneNumber;
    private String eMail;
    private String password;
}
