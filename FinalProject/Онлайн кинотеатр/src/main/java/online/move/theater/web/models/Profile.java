package online.move.theater.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "profiles")
public class Profile {

    public enum Role {
        ADMIN, USER
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String sex;
    private String telephoneNumber;

    @Column(unique = true)
    private String email;

    private String hashPassword;

    @OneToMany(mappedBy = "profile")
    public List<Subscription> subscriptions;

    @OneToMany(mappedBy = "profile")
    public List<BrowsingHistory> browsingHistories;

}
