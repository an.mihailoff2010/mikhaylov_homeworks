package online.move.theater.web.controller;

import lombok.RequiredArgsConstructor;
import online.move.theater.web.forms.SignUpForm;
import online.move.theater.web.services.SignUpService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping
    public String signUpProfile(SignUpForm form) {
        signUpService.signUpProfile(form);
        return "redirect:/signIn";
    }
}
