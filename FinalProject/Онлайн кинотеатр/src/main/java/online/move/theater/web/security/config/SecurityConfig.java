package online.move.theater.web.security.config;

import online.move.theater.web.security.details.ProfileDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ProfileDetailsServiceImpl profileDetailsServiceImpl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(profileDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/profiles").authenticated()
                .antMatchers("/profiles/**").hasAuthority("ADMIN")
                .antMatchers("/signIn").permitAll()
                .and()
                .formLogin()
                .loginPage("/signIn")
                .usernameParameter("eMail")
                .passwordParameter("password")
                .defaultSuccessUrl("/profiles")
                .permitAll();
    }
}
