package online.move.theater.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Catalog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private Integer year_of_production;
    private String style;
    private String country;
    private String duration;
    private String description;

    @ManyToOne
    @JoinColumn(name = "subscription_id")
    public Subscription subscription;

    @OneToMany(mappedBy = "catalog")
    public List<BrowsingHistory> browsingHistories;
}
