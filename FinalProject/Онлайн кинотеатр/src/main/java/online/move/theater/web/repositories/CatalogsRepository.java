package online.move.theater.web.repositories;

import online.move.theater.web.models.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CatalogsRepository extends JpaRepository<Catalog, Integer> {
    List<Catalog> findAllBySubscription_Id(Integer id);
    List<Catalog> findAll();
}
