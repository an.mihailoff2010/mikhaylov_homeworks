package online.move.theater.web.services;

import online.move.theater.web.forms.CatalogForm;
import online.move.theater.web.forms.ProfileForm;
import online.move.theater.web.models.BrowsingHistory;
import online.move.theater.web.models.Catalog;
import online.move.theater.web.models.Profile;
import online.move.theater.web.models.Subscription;

import java.util.List;

public interface ProfilesService {
    void addProfile(ProfileForm form);

    List<Profile> getAllProfiles();

    void deleteProfile(Integer profileId);

    Profile getProfile(Integer profileId);

    void updateProfile(Integer profileId, ProfileForm profileForm);


    List<Subscription> getSubscriptionsByProfile(Integer profileId);

    List<Subscription> getAllSubscriptions();


    List<Catalog> getCatalogsBySubscription(Integer subscriptionId);

    List<Catalog> getAllCatalogs();

    List<BrowsingHistory> getBrowsingHistoriesByProfileAndCatalog(Integer profileId, Integer subscriptionId);

    List<BrowsingHistory> getAllBrowsingHistories();

    void addToCatalog(CatalogForm form);

}
