package online.move.theater.web.services;

import online.move.theater.web.forms.SignUpForm;

public interface SignUpService {
    void signUpProfile(SignUpForm form);

}
