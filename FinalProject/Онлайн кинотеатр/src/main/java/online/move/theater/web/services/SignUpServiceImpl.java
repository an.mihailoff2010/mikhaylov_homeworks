package online.move.theater.web.services;

import lombok.RequiredArgsConstructor;
import online.move.theater.web.forms.SignUpForm;
import online.move.theater.web.models.Profile;
import online.move.theater.web.repositories.ProfilesRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final ProfilesRepository profilesRepository;

    @Override
    public void signUpProfile(SignUpForm form) {
        Profile profile = Profile.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .dateOfBirth(form.getDateOfBirth())
                .sex(form.getSex())
                .telephoneNumber(form.getTelephoneNumber())
                .email(form.getEMail())
                .role(Profile.Role.USER)
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();

        profilesRepository.save(profile);

    }
}
