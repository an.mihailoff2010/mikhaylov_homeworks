package online.move.theater.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name_of_subscription;
    private Double cost;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    public Profile profile;

    @OneToMany(mappedBy = "subscription")
    public List<Catalog> catalogs;

}
