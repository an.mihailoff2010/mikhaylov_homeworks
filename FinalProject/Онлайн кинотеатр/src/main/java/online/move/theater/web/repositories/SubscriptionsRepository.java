package online.move.theater.web.repositories;

import online.move.theater.web.models.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubscriptionsRepository extends JpaRepository<Subscription, Integer> {
    List<Subscription> findAllByProfile_Id(Integer id);
    List<Subscription> findAll();
}
