package online.move.theater.web.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ProfileForm {
    private Integer id;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String dateOfBirth;
    @NotEmpty
    private String sex;
    @NotEmpty
    private String eMail;
    @NotEmpty
    private String telephoneNumber;
}
