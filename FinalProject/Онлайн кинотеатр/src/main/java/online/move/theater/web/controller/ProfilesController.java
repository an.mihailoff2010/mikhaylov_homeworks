package online.move.theater.web.controller;

import online.move.theater.web.forms.CatalogForm;
import online.move.theater.web.forms.ProfileForm;
import online.move.theater.web.models.BrowsingHistory;
import online.move.theater.web.models.Catalog;
import online.move.theater.web.models.Profile;
import online.move.theater.web.models.Subscription;
import online.move.theater.web.services.ProfilesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ProfilesController {

    private final ProfilesService profilesService;

    @Autowired
    public ProfilesController(ProfilesService profilesService) {
        this.profilesService = profilesService;
    }

    @GetMapping("/profiles")
    public String getProfilesPage(Model model) {
        List<Profile> profiles = profilesService.getAllProfiles();
        model.addAttribute("profiles",profiles);
        return "profiles";
    }

    @GetMapping("/profiles/{profile-id}")
    public String getProfilePage(Model model, @PathVariable("profile-id") Integer profileId) {
        Profile profile = profilesService.getProfile(profileId);
        model.addAttribute("profile", profile);
        return "profile";
    }

    @PostMapping("/profiles")
    public String addProfile(@Valid ProfileForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()) {
                forRedirectModel.addFlashAttribute("errors", "Есть ошибки на форме!");
                return "redirect:/profiles";
        }
        profilesService.addProfile(form);
        return "redirect:/profiles";
    }

    @PostMapping("/profiles/{profile-id}/delete")
    public String deleteProfile(@PathVariable("profile-id") Integer profileId) {
        profilesService.deleteProfile(profileId);
        return "redirect:/profiles";
    }

    @PostMapping("/profiles/{profile-id}/update")
    public String updateProfile(@PathVariable("profile-id") Integer profileId, ProfileForm profileForm) {
        profilesService.updateProfile(profileId, profileForm);
        return "redirect:/profiles";
    }

    @GetMapping("/profiles/{profile-id}/all-subscriptions")
    public String getSubscriptionsByProfile(Model model, @PathVariable("profile-id") Integer profileId) {
        List<Subscription> subscriptions = profilesService.getSubscriptionsByProfile(profileId);
        model.addAttribute("subscriptions",subscriptions);
        return "subscriptions_of_profile";
    }

    @GetMapping("/subscriptions_of_profile")
    public String getSubscriptionPage(Model model) {
        List<Subscription> subscriptions = profilesService.getAllSubscriptions();
        model.addAttribute("subscriptions", subscriptions);
        return "subscriptions_of_profile";
    }

    @GetMapping("/subscriptions_of_profile/{subscription-id}/all-catalogs")
    public String getCatalogsBySubscription(Model model, @PathVariable("subscription-id") Integer subscriptionId) {
        List<Catalog> catalogs = profilesService.getCatalogsBySubscription(subscriptionId);
        model.addAttribute("catalogs",catalogs);
        return "catalogs_of_subscription";
    }

    @GetMapping("/catalogs_of_subscription")
    public String getCatalogPage(Model model) {
        List<Catalog> catalogs = profilesService.getAllCatalogs();
        model.addAttribute("catalogs", catalogs);
        return "catalogs_of_subscription";
    }

    @PostMapping("/catalogs_of_subscription")
    public String addToCatalog(CatalogForm form) {
        profilesService.addToCatalog(form);
        return "redirect:/catalogs_of_subscription";
    }

    @GetMapping("/catalogs_of_subscription/{catalog-id}/profile/{profile-id}/all-browsing_histories")
        public String getBrowsingHistoriesByProfileAndCatalog(Model model, @PathVariable("profile-id") Integer profileId, @PathVariable("catalog-id") Integer catalogId) {
        List<BrowsingHistory> browsingHistories = profilesService.getBrowsingHistoriesByProfileAndCatalog(profileId, catalogId);
        model.addAttribute("browsingHistories" , browsingHistories);
        return "browsing_history";
        }

    @GetMapping("/browsing_history")
    public String getBrowsingHistoryPage(Model model) {
        List<BrowsingHistory> browsingHistories = profilesService.getAllBrowsingHistories();
        model.addAttribute("browsingHistories" , browsingHistories);
        return "browsing_history";
    }

}

