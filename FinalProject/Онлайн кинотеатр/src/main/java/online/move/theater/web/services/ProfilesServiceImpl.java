package online.move.theater.web.services;

import lombok.RequiredArgsConstructor;
import online.move.theater.web.exceptions.ProfileNotFoundException;
import online.move.theater.web.forms.CatalogForm;
import online.move.theater.web.forms.ProfileForm;
import online.move.theater.web.models.BrowsingHistory;
import online.move.theater.web.models.Catalog;
import online.move.theater.web.models.Profile;
import online.move.theater.web.models.Subscription;
import online.move.theater.web.repositories.BrowsingHistoriesRepository;
import online.move.theater.web.repositories.CatalogsRepository;
import online.move.theater.web.repositories.ProfilesRepository;
import online.move.theater.web.repositories.SubscriptionsRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class ProfilesServiceImpl implements ProfilesService {

    private final ProfilesRepository profilesRepository;
    private final SubscriptionsRepository subscriptionsRepository;
    private final CatalogsRepository catalogsRepository;
    private final BrowsingHistoriesRepository browsingHistoriesRepository;

    @Override
    public void addProfile(ProfileForm form) {
        Profile profile = Profile.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .dateOfBirth(form.getDateOfBirth())
                .sex(form.getSex())
                .telephoneNumber(form.getTelephoneNumber())
                .email(form.getEMail())
                .build();

        profilesRepository.save(profile);
    }

    @Override
    public List<Profile> getAllProfiles() {
        return profilesRepository.findAll();
    }

    @Override
    public void deleteProfile(Integer profileId) {
        profilesRepository.deleteById(profileId);
    }

    @Override
    public Profile getProfile(Integer profileId) {
        return profilesRepository.findById(profileId).orElseThrow(ProfileNotFoundException::new);
    }

    @Override
    public void updateProfile(Integer profileId, ProfileForm profileForm) {
        Profile profile = profilesRepository.getById(profileId);
        profile.setFirstName(profileForm.getFirstName());
        profile.setLastName(profileForm.getLastName());
        profile.setDateOfBirth(profileForm.getDateOfBirth());
        profile.setSex(profileForm.getSex());
        profile.setTelephoneNumber(profileForm.getTelephoneNumber());
        profile.setEmail(profileForm.getEMail());

        profilesRepository.save(profile);
    }

    @Override
    public List<Subscription> getSubscriptionsByProfile(Integer profileId) {
        return subscriptionsRepository.findAllByProfile_Id(profileId);
    }

    @Override
    public List<Subscription> getAllSubscriptions() {
        return subscriptionsRepository.findAll();
    }

    @Override
    public List<Catalog> getCatalogsBySubscription(Integer subscriptionId) {
        return catalogsRepository.findAllBySubscription_Id(subscriptionId);
    }

    @Override
    public List<Catalog> getAllCatalogs() {
        return catalogsRepository.findAll();
    }

    @Override
    public List<BrowsingHistory> getBrowsingHistoriesByProfileAndCatalog(Integer profileId, Integer catalogId) {
        return browsingHistoriesRepository.findAllByProfile_IdAndCatalog_Id(profileId, catalogId);
    }

    @Override
    public List<BrowsingHistory> getAllBrowsingHistories() {
        return browsingHistoriesRepository.findAll();
    }

    @Override
    public void addToCatalog(CatalogForm form) {
        Catalog catalog = Catalog.builder()
                .name(form.getName())
                .year_of_production(form.getYear_of_production())
                .style(form.getStyle())
                .country(form.getCountry())
                .duration(form.getDuration())
                .description(form.getDescription())
                .build();

        catalogsRepository.save(catalog);

    }

}
