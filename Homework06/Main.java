package com.company;

public class Main {

    public static int getIndexNumber(int[] array, int number) {
        int count = 0;
        for (int i = 0; i < array.length; i++)
        {
            if(array[i] == number) {
                return i;
            }
        }
        return -1;
    }

    public static void print(int[] array) {
        int pos = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                array[pos] = array[i];
                pos++;
                System.out.print(array[i] + " ");
            }
        }
        for (int i = pos; i < array.length; i++) {
            array[pos++] = 0;
            System.out.print(array[i] + " ");
        }
    }

    public static void main(String[] args) {
        int[] a = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int result = getIndexNumber(a, 14);
        System.out.println(result);
        print(a);
    }
}