package homework;

public class NumbersUtil {

    //Домашняя работа

    /*
    нод(18, 12) -> 6
    нод(9, 12) -> 3
    нод(64, 48) -> 16

    Предусмотреть, когда на вход "некрсивые числа", отрицательные числа -> исключения
     */
    public int gcd(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
            if (a < 0 || b < 0) {
                throw new IllegalArgumentException();
            }
        }
        return a;
    }
}
