package homework;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    // то, что мы будем тестировать
    private final NumbersUtil numbersUtil = new NumbersUtil();

    //Домашняя работа
    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {
        @ParameterizedTest(name = "return {2} on gcd {0} and {1}")
        @CsvSource(value = {"18, 12, 6", "9, 12, 3", "64, 48, 16"})
        public void return_correct_gcd(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception on {0} and {1}")
        @CsvSource(value = {"-18, 12", "9, -12", "-64, -48"})
        public void negative_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }
    }
}