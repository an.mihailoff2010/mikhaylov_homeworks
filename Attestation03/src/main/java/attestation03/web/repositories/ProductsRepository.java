package attestation03.web.repositories;

import attestation03.web.products.Product;

import java.util.List;


public interface ProductsRepository {
    List<Product> findAll();
    List<Product> findAllByPrice(double price);
    void save(Product product);
}
