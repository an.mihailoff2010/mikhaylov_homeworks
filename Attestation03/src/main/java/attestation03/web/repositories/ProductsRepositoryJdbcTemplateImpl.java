package attestation03.web.repositories;

import attestation03.web.products.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from products order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from products where price = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into products(product_name, price, quantity) values(?, ?, ?)";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {

        int id = row.getInt("id");
        String productName = row.getString("product_name");
        double price = row.getDouble("price");
        int quantity = row.getInt("quantity");

        return new Product(id, productName, price, quantity);
    };


    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper,price);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getProductName(), product.getPrice(), product.getQuantity());
    }


}
