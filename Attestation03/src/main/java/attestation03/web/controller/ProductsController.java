package attestation03.web.controller;

import attestation03.web.products.Product;
import attestation03.web.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductsController {
    //DI
    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("productName") String productName,
                             @RequestParam("price") double price) {

        Product product = Product.builder()
                .productName(productName)
                .price(price)
                .build();

        productsRepository.save(product);

        return "redirect:/products_add.html";
    }
}
