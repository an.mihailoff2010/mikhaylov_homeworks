package homework20;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader("auto.txt"))) {
            reader.lines()
                    .map(line -> line.split("\\|"))
                    .filter(arr -> "Black".equalsIgnoreCase(arr[2])
                            || 0 == Integer.parseInt(arr[3])
                    )
                    .forEach(arr -> System.out.println(arr[0]));
            long countFrom700to800 = reader.lines()
                    .map(line -> line.split("\\|"))
                    .filter(arr -> 700_000 <= Integer.parseInt(arr[4])
                            && Integer.parseInt(arr[4]) < 800_000
                    )
                    .map(arr -> arr[1])
                    .distinct()
                    .count();
            System.out.println(countFrom700to800);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

