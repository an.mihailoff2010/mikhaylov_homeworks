import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int minDigit = number % 10;
        while (number != -1) {
            if (number == 0) {
                minDigit = 0;
            }
                while (number != 0) {
                    int curDigit = number % 10;
                    if (curDigit < minDigit) {
                        minDigit = curDigit;
                    }
                    number /= 10;
                }
            number = scanner.nextInt();
            }
        System.out.println("Минимальная цифра " + minDigit);
    }
}