package homework11;

public class Logger {

    private static final Logger logger;

    static {
        logger = new Logger();
    }

    private Logger() {
        this.message = new String();
    }

    public static Logger getInstance() {
        return logger;
    }

    private String message;

    public void log(String message) {
        this.message = message;
    }

    public String log2(String message) {
        return this.message;
    }
}
