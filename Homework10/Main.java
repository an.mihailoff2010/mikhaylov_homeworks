package homework10;

public class Main {

    public static void main(String[] args) {
        Square square = new Square(5,8,6);
        Circle circle = new Circle(9, 7,6);

        MovableFigure[] movableFigures = new MovableFigure[2];
        movableFigures[0] = square;
        movableFigures[1] = circle;

        for (MovableFigure movableFigure : movableFigures) {
            movableFigure.getMove(4,6);
        }
        printX(square, circle);
        for(MovableFigure movableFigure : movableFigures) {
            System.out.println("Периметр " + ((Figure) movableFigure).getName() + ((Figure)movableFigure).getPerimeter());
        }
    }

    private static void printX(Figure...movableFigures) {
        for (Figure figure : movableFigures) {
            System.out.println("Новые координаты центра " + figure.getName() + " - " + figure.x + ", " + figure.y);
        }
    }
}
