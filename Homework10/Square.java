package homework10;

public class Square extends Rectangle implements MovableFigure {

    public Square(int a, int x, int y) {
        super(a, a, x, y);
    }

    @Override
    public String getName() {
        return "Квадрата ";
    }

    @Override
    public void getMove(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

