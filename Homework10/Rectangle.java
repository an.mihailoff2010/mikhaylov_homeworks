package homework10;

public class Rectangle extends Figure {
    private int a;
    private int b;

    public Rectangle(int a, int b, int x, int y) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public int getPerimeter() {
        return (a * 2 + b * 2);
    }
}
