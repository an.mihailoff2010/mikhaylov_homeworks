package homework10;

public class Circle extends Ellipse implements MovableFigure {

    public Circle(int radiusA, int x, int y) {
        super(radiusA, radiusA, x, y);
    }

    @Override
    public String getName() {
        return "Круга ";
    }

    @Override
    public void getMove(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
