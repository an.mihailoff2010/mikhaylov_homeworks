package homework10;

public abstract class Figure {
    protected int x;
    protected int y;
    protected String name;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getPerimeter() {
        return 0;
    }

    public String getName() {
        return name;
    }
}
