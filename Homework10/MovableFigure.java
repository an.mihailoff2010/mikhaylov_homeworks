package homework10;

public interface MovableFigure {
    void getMove(int x, int y);
}
