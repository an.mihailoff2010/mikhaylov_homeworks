package homework10;

public class Ellipse extends Figure {
    private int radiusA;
    private int radiusB;

    public Ellipse(int radiusA, int radiusB, int x, int y) {
        super(x, y);
        this.radiusA = radiusA;
        this.radiusB = radiusB;
    }

    public int getPerimeter() {
        return (int) ((2 * Math.PI) * Math.sqrt(((radiusA * radiusA) + (radiusB * radiusB))/2));
    }
}
