package com.company;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 15);
        Square square = new Square(5);
        Ellipse ellipse = new Ellipse(3, 5);
        Circle circle = new Circle(3);

        System.out.println("Периметр прямоугольника " + rectangle.getPerimeter());
        System.out.println("Периметр квадрата " + square.getPerimeter());
        System.out.println("Периметр эллипса " + ellipse.getPerimeter());
        System.out.println("Периметр круга " + circle.getPerimeter());
    }
}
