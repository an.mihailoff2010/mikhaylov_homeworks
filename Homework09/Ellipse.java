package com.company;

public class Ellipse extends Figure {
    public Ellipse(double x, double y) {
        super(x, y);
    }

    public double getPerimeter() {
        return (2 * Math.PI) * Math.sqrt(((x * x) + (y * y))/2);
    }
}
