package homework17;


import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	    String string = "На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.";
        String[] a = string.split(" ");
        Map<String, Integer> map = new HashMap<>();
        for (String str : a) {
            if (map.containsKey(str)) {
                map.put(str, 1 + map.get(str));
            } else {
                map.put(str, 1);
            }
        }
        System.out.println(map);
    }
}
