package com.company;
import java.util.Scanner;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int[] results = new int[201];

        //считываем число и если оно в нашем диапазоне увеличиваем счетчик на 1
        int number = console.nextInt();
        while (number != -1) {
            for (int i = 0; i < results.length; i++) {
                if (number == (i - 100)) {
                    results[i]++;
                }
            }
            number = console.nextInt();
        }

        //находим минимальное число больше нуля в массиве-счетчике
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < results.length; i++) {
            if (results[i] < min && results[i] > 0) {
                min = results[i];
            }
        }

        System.out.println(Arrays.toString(results));
        System.out.println(min);
    }
}