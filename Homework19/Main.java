package homework19;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        List<User> users2 = usersRepository.findByAge(22);
        List<User> users3 = usersRepository.findByIsWorkerIsTrue();


        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        for (User user : users2) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        for (User user : users3) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }


        User user = new User("Игорь", 33, true);
        usersRepository.save(user);
    }
}
