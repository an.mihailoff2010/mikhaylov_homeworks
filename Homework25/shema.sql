create table products (
                          id serial primary key,
                          product_name varchar(20),
                          price numeric,
                          quantity integer
);

insert into products(product_name, price, quantity) values ('Телевизор', 20000.00, 30);
insert into products(product_name, price, quantity) values ('Ноутбук', 50000.00, 26);
insert into products(product_name, price, quantity) values ('Холодильник', 15000.00, 35);
insert into products(product_name, price, quantity) values ('Смартфон', 30000.00, 40);

create table customer (
                          id serial primary key,
                          first_name varchar(20),
                          last_name varchar(20)
);

insert into customer(first_name, last_name) values ('Марсель', 'Сидиков');
insert into customer(first_name, last_name) values ('Виктор', 'Евлампьев');
insert into customer(first_name, last_name) values ('Максим', 'Поздеев');
insert into customer(first_name, last_name) values ('Антон', 'Михайлов');

create table reservation (
                             owner_id1 integer,
                             foreign key(owner_id1) references products(id),
                             owner_id2 integer,
                             foreign key(owner_id2) references customer(id),
                             reservation_date DATE,
                             quantity_of_product integer
);

insert into reservation(owner_id1, owner_id2, reservation_date, quantity_of_product) values (1,1,'2021-11-15',2);
insert into reservation(owner_id1, owner_id2, reservation_date, quantity_of_product) values (2,2,'2021-11-12',1);
insert into reservation(owner_id1, owner_id2, reservation_date, quantity_of_product) values (3,3,'2021-11-20',1);
insert into reservation(owner_id1, owner_id2, reservation_date, quantity_of_product) values (4,4,'2021-11-27',2);
