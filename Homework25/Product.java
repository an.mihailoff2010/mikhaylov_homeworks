package homework;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private int id;
    private String productName;
    private double price;
    private int quantity;
}
