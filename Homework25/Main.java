package homework;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/postgres",
                "postgres", "05081994");

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findAllByPrice(50000.0));


    }
}
