package homework;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from products order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from products where price = ?";

    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {

        int id = row.getInt("id");
        String productName = row.getString("product_name");
        double price = row.getDouble("price");
        int quantity = row.getInt("quantity");

        return new Product(id, productName, price, quantity);
    };


    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper,price);
    }


}
